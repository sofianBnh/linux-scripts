# Ubuntu Scripts

## Description

A small list of scripts for general administration task on Ubuntu (could be extended later). 
This includes:

- **Basics**: basic setups like `ssh configuration`, `useful tools installation`, `add serial console`...

- **Docker**: scripts to install `docker`, `docker-compose`, `kubernetes`...

- **Environment**: Setup programming language environments like `java`, `python` for Data Science, `go`...

- **Networking**: General use case networking setups.