#!/bin/bash
# Source https://docs.docker.com/install/linux/docker-ce/ubuntu/

set -xe 

apt-get -y remove docker docker-engine docker.io
apt-get update
apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository  \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" -y
apt-get update
apt-get install -y docker-ce
