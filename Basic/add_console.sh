#!/bin/bash
# Source https://www.hiroom2.com/2018/04/30/ubuntu-1804-serial-console-en/

set -xe 

GRUB_CONFIG_FILE=/etc/default/grub

# Comment Original Values
sed -i '/GRUB_DEFAULT/s/^/#/g' $GRUB_CONFIG_FILE
sed -i '/GRUB_TIMEOUT/s/^/#/g' $GRUB_CONFIG_FILE
sed -i '/GRUB_TERMINAL/s/^/#/g' $GRUB_CONFIG_FILE
sed -i '/GRUB_DISTRIBUTOR/s/^/#/g' $GRUB_CONFIG_FILE
sed -i '/GRUB_CMDLINE_LINUX/s/^/#/g' $GRUB_CONFIG_FILE
sed -i '/GRUB_SERIAL_COMMAND/s/^/#/g' $GRUB_CONFIG_FILE
sed -i '/GRUB_CMDLINE_LINUX_DEFAULT/s/^/#/g' $GRUB_CONFIG_FILE

# Add new Values
cat<<EOF >>$GRUB_CONFIG_FILE

GRUB_DEFAULT=0
GRUB_TIMEOUT=1
GRUB_TERMINAL="console serial"
GRUB_DISTRIBUTOR=\`lsb_release -i -s 2> /dev/null || echo Debian\`
GRUB_CMDLINE_LINUX="console=tty1 console=ttyS0,115200"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
GRUB_CMDLINE_LINUX_DEFAULT=""
EOF

sed -i 's/ rhgb quiet//' $GRUB_CONFIG_FILE

# Update the grub
update-grub2
reboot
