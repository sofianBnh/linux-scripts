#!/bin/bash

set -xe

# Updates
apt update && apt upgrade -y && apt dist-upgrade -y

# Essentails
apt install zsh vim htop git curl fonts-powerline -y

# ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sed -i 's/robbyrussell/agnoster/g' ~/.zshrc
