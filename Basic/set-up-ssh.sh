#!/bin/bash
# Source https://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html

cat > /etc/ssh/sshd_config <<EOF
PermitRootLogin no
ChallengeResponseAuthentication no
# PasswordAuthentication no # After ssh-copy-id
UsePAM no

AuthenticationMethods publickey
PubkeyAuthentication yes 

PermitEmptyPasswords no

ClientAliveInterval 300
ClientAliveCountMax 0
EOF

systemctl restart ssh
