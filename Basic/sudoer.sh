#!/bin/bash

if [ $# -ne "1" ]; then
    echo "No user specified"
    return
fi 

read -p "Do you want to make the user $1 a no password sudoer [y/n]? " -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "$1 ALL=(ALL) NOPASSWD:ALL" | sudo EDITOR='tee -a' visudo   
else
    echo "Aborting"
fi
